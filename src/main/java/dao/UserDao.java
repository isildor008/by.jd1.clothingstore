package dao;

import model.entuty.User;

import java.util.List;

/**
 * Created by Dmitry on 04.06.2017.
 */
public interface UserDao {

    User fetchByCredentials(String login, String password);
    List<User> getUserList();

}