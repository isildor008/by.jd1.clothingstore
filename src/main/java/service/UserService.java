package service;

import model.entuty.User;

import java.util.List;

/**
 * Created by Dmitry on 05.06.2017.
 */
public interface UserService {
    User authotise(String login,String password);
    boolean logOut(User user);
    List<User> list();
}
