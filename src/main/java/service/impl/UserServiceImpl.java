package service.impl;
import dao.UserDao;
import dao.impl.UserDaoImpl;
import model.entuty.User;
import service.UserService;
import java.util.List;

/**
 * Created by Dmitry on 05.06.2017.
 */
public class UserServiceImpl implements UserService {

    private UserDao dao;

    public UserServiceImpl(){
        dao=new UserDaoImpl();
    }

    public User authotise(String login, String password){
        User user = dao.fetchByCredentials(login, password);
        if (user == null){
        }
        return user;
    }

    public boolean logOut(User user) {
        return false;
    }

    public List<User> list() {
        return dao.getUserList();
    }
}
