package command;

import static util.ConstantValue.ACTION_AUTHORISE;

/**
 * Created by Dmitry on 05.06.2017.
 */
public class CommandChoser {
    public static CommandAction chooseAction(String action){
        if (ACTION_AUTHORISE.equals(action)) {

            System.out.println("Login action " + action);
            return new LoginCommandAction();
        }
        if ("start".equals(action)) {

            System.out.println("start " + action);
            return new HomePage();
        }
        if ("login".equals(action)) {

            System.out.println("login " + action);
            return new LoginCommandAction();
        }
        if ("card".equals(action)) {

            System.out.println("card " + action);
            return new CardAction();
        }
        if ("contact".equals(action)) {

            System.out.println("contact " + action);
            return new ContactAction();
        }
        if ("shop".equals(action)) {

            System.out.println("shop " + action);
            return new ShopAction();
        }

        return null;
    }
}
