package command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Dmitry on 06.06.2017.
 */
public class ShopAction implements CommandAction {
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        return "/WEB-INF/page/shopPage.jsp";
    }
}
